package com.middlfy.meetmeinthemiddle;

import android.text.TextUtils;

import com.yelp.clientlib.entities.Business;

import java.util.ArrayList;

public class YelpResult {
    public String ImageUrl;
    public String Rating;
    public String Address;
    public String FullAddress;
    public String MapAddress;
    public String Name;
    public String PhoneNumber;
    public String ReviewCount;
    public String RatingImageUrl;
    public String YelpUrl;

    public YelpResult(){}

    public YelpResult(Business business){
        PhoneNumber = business.displayPhone();
        ImageUrl = business.imageUrl();
        Rating = business.rating().toString();
        FullAddress = TextUtils.join(", ",business.location().displayAddress());
        Name = business.name();
        Address = Name + ", " + TextUtils.join(", ",business.location().address());
        ReviewCount = business.reviewCount().toString();
        RatingImageUrl = business.ratingImgUrlLarge();
        YelpUrl = business.id();
        MapAddress = Name + ", " + FullAddress;

        if(TextUtils.isEmpty(Address)){
            Address = "N/A";
        }

        if(TextUtils.isEmpty(PhoneNumber)){
            PhoneNumber = "N/A";
        }
    }

    public static YelpResultList UpdateYelpResultViewModel(ArrayList<Business> businesses){
        YelpResultList list = new YelpResultList();

        for(int i = 0; i< businesses.size(); i++){
            list.add(new YelpResult(businesses.get(i)));
        }

        return  list;
    }
}
