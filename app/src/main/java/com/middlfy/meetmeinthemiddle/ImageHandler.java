package com.middlfy.meetmeinthemiddle;

import android.content.Context;

import com.squareup.picasso.Cache;
import com.squareup.picasso.Picasso;

import java.util.concurrent.Executors;

public class ImageHandler {

    private static Picasso instance;

    public static void setSharedInstance(Context context){
        instance = new Picasso.Builder(context).executor(Executors.newSingleThreadExecutor()).memoryCache(Cache.NONE).indicatorsEnabled(true).build();
        instance.setIndicatorsEnabled(false);
    }

    public  static Picasso Instance(){
        return instance;
    }
}
