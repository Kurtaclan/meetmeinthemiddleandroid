package com.middlfy.meetmeinthemiddle;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.DialogFragment;

public class YelpResultHandler {
    private Context context;

    public YelpResultHandler(Context context) {
        this.context = context;
    }

    public void onResultClick(YelpResult result) {
        YelpController.SetYelpResult(result);
        DialogFragment frag = YelpResultDialog.newInstance(result.Name);
        frag.show(YelpController.Instance().CurrentFragmentManager, "dialog");
    }

    public void onAddressClick(String address){
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.co.in/maps?q=" + address));
        context.startActivity(i);
    }

    public void onYelpAddressClick(String yelpId){
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.yelp.com/biz/" + yelpId));
        context.startActivity(i);
    }
}
