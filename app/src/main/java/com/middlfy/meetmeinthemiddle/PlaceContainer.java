package com.middlfy.meetmeinthemiddle;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Peter on 2/18/2017.
 */

public class PlaceContainer {
    LatLng latLng;
    int index;

    public PlaceContainer(int index, LatLng latLng) {
        this.index = index;
        this.latLng = latLng;
    }

    public  PlaceContainer(int index){
        this.index = index;
    }

    @Override
    public boolean  equals (Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
            result = false;
        } else {
            PlaceContainer place = (PlaceContainer) object;
            if (this.index == place.index) {
                result = true;
            }
        }
        return result;
    }
}
