package com.middlfy.meetmeinthemiddle;
import android.databinding.ObservableArrayList;
import android.view.View;

public class YelpResultList {
    public ObservableArrayList<YelpResult> list = new ObservableArrayList<>();

    public void add(YelpResult info) {
        list.add(info);
    }
}