package com.middlfy.meetmeinthemiddle;

import android.content.Context;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;

/**
 * Created by Peter on 2/22/2017.
 */
public class Logger {
    private static Logger ourInstance = new Logger();

    public static Logger Instance() {
        return ourInstance;
    }

    public void appendLog(String text, Context context)
    {
        File logFile = new File(context.getFilesDir(),"middleLog.log");

        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private Logger() {
    }
}
