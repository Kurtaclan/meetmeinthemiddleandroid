package com.middlfy.meetmeinthemiddle;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.middlfy.meetmeinthemiddle.databinding.LayoutYelpResultBinding;


public class ListAdapter extends BaseAdapter {
    private ObservableArrayList<YelpResult> list;
    private LayoutInflater inflater;

    public ListAdapter(ObservableArrayList<YelpResult> l) {
        list = l;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        LayoutYelpResultBinding binding = DataBindingUtil.inflate(inflater, R.layout.layout_yelp_result, parent, false);
        binding.setItem(list.get(position));
        binding.setHandler(new YelpResultHandler(parent.getContext()));
        ImageHandler.Instance().load(list.get(position).ImageUrl).into(binding.imageView2);
        return binding.getRoot();
    }
}