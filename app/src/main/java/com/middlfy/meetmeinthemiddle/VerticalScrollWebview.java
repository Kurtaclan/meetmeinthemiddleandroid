package com.middlfy.meetmeinthemiddle;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;

/**
 * custom {@link WebView} which only scrolls vertically but not horizontally
 */
public class VerticalScrollWebview extends WebView implements View.OnTouchListener {

    // the initial touch points x coordinate
    private float touchDownX;

    public VerticalScrollWebview(Context context) {
        super(context);

        // make vertically scrollable only
        makeVerticalScrollOnly();
    }

    public VerticalScrollWebview(Context context, AttributeSet attrs) {
        super(context, attrs);

        // make vertically scrollable only
        makeVerticalScrollOnly();
    }

    public VerticalScrollWebview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // make vertically scrollable only
        makeVerticalScrollOnly();
    }

    /**
     * make this {@link WebView} vertically scroll only
     */
    private void makeVerticalScrollOnly() {
        // set onTouchListener for vertical scroll only
        setOnTouchListener(this);

        // hide horizontal scroll bar
        setHorizontalScrollBarEnabled(false);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        // multitouch is ignored
        if (motionEvent.getPointerCount() > 1) {
            return true;
        }

        // handle x coordinate on single touch events
        switch (motionEvent.getAction()) {
            // save the x coordinate on touch down
            case MotionEvent.ACTION_DOWN:
                touchDownX = motionEvent.getX();
                break;

            // reset the x coordinate on each other touch action, so it doesn't move
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                motionEvent.setLocation(touchDownX, motionEvent.getY());
                break;

        }

        // let the super view handle the update
        return false;

    }
}