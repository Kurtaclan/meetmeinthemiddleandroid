package com.middlfy.meetmeinthemiddle;

import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.widget.ImageView;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

public class ListBinder {
    @BindingAdapter("bind:items")
    public static void bindList(ListView view, ObservableArrayList<YelpResult> list) {
        ListAdapter adapter = new ListAdapter(list);
        view.setAdapter(adapter);
    }
}