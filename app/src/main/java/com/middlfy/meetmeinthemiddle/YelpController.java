package com.middlfy.meetmeinthemiddle;


import android.support.v4.app.FragmentManager;

public class YelpController {
    public YelpResult SelectedYelpResult;
    public FragmentManager CurrentFragmentManager;

    private static YelpController ourInstance = new YelpController();

    public static YelpController Instance() {
        return ourInstance;
    }

    public static void SetYelpResult(YelpResult yelpResult){
        Instance().SelectedYelpResult = yelpResult;
    }

    public static void SetFragmentManager(FragmentManager fragmentManager){
        Instance().CurrentFragmentManager = fragmentManager;
    }
}
