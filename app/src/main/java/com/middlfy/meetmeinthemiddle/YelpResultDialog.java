package com.middlfy.meetmeinthemiddle;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.middlfy.meetmeinthemiddle.databinding.LayoutYelpResultDialogBinding;

public class YelpResultDialog extends DialogFragment {
    public static YelpResultDialog newInstance(String title) {
        YelpResultDialog frag = new YelpResultDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);

        return frag;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");

        FragmentActivity activity = getActivity();

        LayoutYelpResultDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.layout_yelp_result_dialog, null, false);

        ImageHandler.Instance().load(YelpController.Instance().SelectedYelpResult.ImageUrl).into(binding.dialogImage);
        ImageHandler.Instance().load(YelpController.Instance().SelectedYelpResult.RatingImageUrl).resize(140, 25).into(binding.rating);
        binding.setYelpResult(YelpController.Instance().SelectedYelpResult);
        binding.setHandler(new YelpResultHandler(activity));
        binding.dialogAddress.setPaintFlags(binding.dialogAddress.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        binding.dialogShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubmitYelpShare(v);
            }
        });

        return new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle)
                .setTitle(title)
                .setView(binding.getRoot())
                .create();
    }

    public void SubmitYelpShare(View v) {
        EditText text = (EditText)v.getRootView().findViewById(R.id.dialogMessageBody);
        Intent myIntent = new Intent(Intent.ACTION_SEND);
        myIntent.setType("text/plain");
        String shareBody = text.getText().toString();
        String shareSub = "Sent by Meet Me In the Middle.";
        myIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
        myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(myIntent, "Share using"));
    }
}
