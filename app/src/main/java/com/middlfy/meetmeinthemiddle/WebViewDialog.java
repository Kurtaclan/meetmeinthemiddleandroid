package com.middlfy.meetmeinthemiddle;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.middlfy.meetmeinthemiddle.databinding.LayoutYelpResultDialogBinding;


public class WebViewDialog extends DialogFragment {
    public static WebViewDialog newInstance(String title, String url) {
        WebViewDialog frag = new WebViewDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("url", url);
        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String url = getArguments().getString("url");
        FragmentActivity activity = getActivity();
        LayoutInflater inflater = getActivity().getLayoutInflater();

        WebView wv = (WebView)inflater.inflate(R.layout.content_webview_dialog, null);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl(url);

        return new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle)
                .setTitle(title)
                .setView(wv)
                .create();
    }
}
