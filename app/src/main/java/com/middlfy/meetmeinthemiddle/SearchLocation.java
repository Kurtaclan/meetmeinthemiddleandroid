package com.middlfy.meetmeinthemiddle;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.edmodo.rangebar.RangeBar;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import  com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.middlfy.meetmeinthemiddle.databinding.ContentYelpResultsBinding;
import com.middlfy.meetmeinthemiddle.databinding.LayoutYelpResultBinding;
import com.squareup.picasso.Picasso;
import com.yelp.clientlib.connection.YelpAPI;
import com.yelp.clientlib.connection.YelpAPIFactory;
import com.yelp.clientlib.entities.SearchResponse;
import com.yelp.clientlib.entities.options.BoundingBoxOptions;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchLocation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    YelpAPIFactory apiFactory;
    YelpAPI yelpAPI;
    ViewFlipper flipper;
    static String YELP_CONSUMER_KEY = "dI1PJBWremFdBhhnnGUmqw";
    static String YELP_CONSUMER_SECRET = "I5cbkOcvcAe7i6k8sCQNo16LHn0";
    static String YELP_TOKEN = "JojR6irtUVFcN0-It2QfOwPwWGz-uqBw";
    static String YELP_TOKEN_SECRET = "uQrFJLoCq7ohe3320bhIJT0j2Lw";
    double SW_Latitude;
    double SW_Longitude;
    double NE_Latitude ;
    double NE_Longitude;
    LayoutInflater inflater;

    private void showNext(){
        flipper.setInAnimation(this, R.anim.slide_in);
        flipper.setOutAnimation(this, R.anim.slide_out);
        flipper.showNext();
    }

    private  void showPrevious(){
        flipper.setInAnimation(this, R.anim.slide_in_back);
        flipper.setOutAnimation(this, R.anim.slide_out_back);
        flipper.showPrevious();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageHandler.setSharedInstance(getApplicationContext());
        YelpController.SetFragmentManager(getSupportFragmentManager());
        setContentView(R.layout.activity_search_location);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        flipper = (ViewFlipper) findViewById(R.id.vf);
        setSupportActionBar(toolbar);

        Spinner spinner = (Spinner) findViewById(R.id.peopleSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.person_select_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        apiFactory = new YelpAPIFactory(YELP_CONSUMER_KEY, YELP_CONSUMER_SECRET, YELP_TOKEN, YELP_TOKEN_SECRET);
        yelpAPI =  apiFactory.createAPI();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        yelpQuery = (EditText) findViewById(R.id.yelpQuery_search);
      /*yelpQuery.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = yelpQuery.getRight()
                            - yelpQuery.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    // when EditBox has padding, adjust leftEdge like
                    // leftEdgeOfRightDrawable -= getResources().getDimension(R.dimen.edittext_padding_left_right);
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        yelpQuery.setText("");
                        return true;
                    }
                }
                return false;
            }
        });
        */

        /*
        yelpQuery = (EditText) findViewById(R.id.yelpQuery);
        ratingBar = (RangeBar) findViewById(R.id.ratingBar);
        ratingBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {
                rating = "";

                if(leftThumbIndex == 0 && rightThumbIndex == 4)
                {
                    return;
                }

                for(int i = leftThumbIndex; i<=rightThumbIndex; i++) {
                    rating += Integer.toString((i + 1)) + " ";
                }

                rating = rating.trim();
            }
        });

        priceBar = (RangeBar) findViewById(R.id.priceBar);
        priceBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {
                price = "";

                if(leftThumbIndex == 0 && rightThumbIndex == 4)
                {
                    return;
                }

                for(int i = leftThumbIndex; i<=rightThumbIndex; i++) {
                    price += Integer.toString((i + 1)) + " ";
                }

                price = price.trim();
            }
        });
        */
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                autocompleteFragmentContainer3.setVisibility(View.GONE);
                autocompleteFragmentContainer4.setVisibility(View.GONE);
                autocompleteFragmentContainer5.setVisibility(View.GONE);
                autocompleteFragmentContainer6.setVisibility(View.GONE);
                autocompleteFragmentContainer7.setVisibility(View.GONE);
                autocompleteFragmentContainer8.setVisibility(View.GONE);
                autocompleteFragmentContainer9.setVisibility(View.GONE);
                autocompleteFragmentContainer10.setVisibility(View.GONE);

                if(position == 8) {
                    autocompleteFragmentContainer10.setVisibility(View.VISIBLE);
                }
                if(position >= 7){
                    autocompleteFragmentContainer9.setVisibility(View.VISIBLE);
                }
                if(position >= 6){
                    autocompleteFragmentContainer8.setVisibility(View.VISIBLE);
                }
                if(position >= 5){
                    autocompleteFragmentContainer7.setVisibility(View.VISIBLE);
                }
                if(position >= 4){
                    autocompleteFragmentContainer6.setVisibility(View.VISIBLE);
                }
                if(position >= 3){
                    autocompleteFragmentContainer5.setVisibility(View.VISIBLE);
                }
                if(position >= 2){
                    autocompleteFragmentContainer4.setVisibility(View.VISIBLE);
                }
                if(position >= 1){
                    autocompleteFragmentContainer3.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        inflater = getLayoutInflater();
        InitializePlaces();
    }

    View autocompleteFragmentContainer1;
    View autocompleteFragmentContainer2;
    View autocompleteFragmentContainer3;
    View autocompleteFragmentContainer4;
    View autocompleteFragmentContainer5;
    View autocompleteFragmentContainer6;
    View autocompleteFragmentContainer7;
    View autocompleteFragmentContainer8;
    View autocompleteFragmentContainer9;
    View autocompleteFragmentContainer10;

    PlaceAutocompleteFragment autocompleteFragment1;
    PlaceAutocompleteFragment autocompleteFragment2;
    PlaceAutocompleteFragment autocompleteFragment3;
    PlaceAutocompleteFragment autocompleteFragment4;
    PlaceAutocompleteFragment autocompleteFragment5;
    PlaceAutocompleteFragment autocompleteFragment6;
    PlaceAutocompleteFragment autocompleteFragment7;
    PlaceAutocompleteFragment autocompleteFragment8;
    PlaceAutocompleteFragment autocompleteFragment9;
    PlaceAutocompleteFragment autocompleteFragment10;

    public void InitializePlaces(){
        autocompleteFragmentContainer1 = findViewById(R.id.places_container_1);
        autocompleteFragment1 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_1);

        autocompleteFragment1.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(1, place);
                af1.setError(null);
            }

            @Override
            public void onError(Status status) {
            }
        });

        autocompleteFragmentContainer2 = findViewById(R.id.places_container_2);
        autocompleteFragment2 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_2);

        autocompleteFragment2.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(2, place);
                af2.setError(null);
            }

            @Override
            public void onError(Status status) {

            }
        });

        autocompleteFragmentContainer3 = findViewById(R.id.places_container_3);
        autocompleteFragment3 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_3);

        autocompleteFragment3.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(3, place);
                af3.setError(null);
            }

            @Override
            public void onError(Status status) {

            }
        });

        autocompleteFragmentContainer4 = findViewById(R.id.places_container_4);
        autocompleteFragment4 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_4);

        autocompleteFragment4.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(4, place);
                af4.setError(null);
            }

            @Override
            public void onError(Status status) {

            }
        });

        autocompleteFragmentContainer5 = findViewById(R.id.places_container_5);
        autocompleteFragment5 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_5);

        autocompleteFragment5.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(5, place);
                af5.setError(null);
            }

            @Override
            public void onError(Status status) {

            }
        });

        autocompleteFragmentContainer6 = findViewById(R.id.places_container_6);
        autocompleteFragment6 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_6);

        autocompleteFragment6.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(6, place);
                af6.setError(null);
            }

            @Override
            public void onError(Status status) {

            }
        });

        autocompleteFragmentContainer7 = findViewById(R.id.places_container_7);
        autocompleteFragment7 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_7);

        autocompleteFragment7.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(7, place);
                af7.setError(null);
            }

            @Override
            public void onError(Status status) {

            }
        });

        autocompleteFragmentContainer8 = findViewById(R.id.places_container_8);
        autocompleteFragment8 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_8);

        autocompleteFragment8.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(8, place);
                af8.setError(null);
            }

            @Override
            public void onError(Status status) {

            }
        });

        autocompleteFragmentContainer9 = findViewById(R.id.places_container_9);
        autocompleteFragment9 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_9);

        autocompleteFragment9.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(9, place);
                af9.setError(null);
            }

            @Override
            public void onError(Status status) {

            }
        });

        autocompleteFragmentContainer10 = findViewById(R.id.places_container_10);
        autocompleteFragment10 = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.places_10);

        autocompleteFragment10.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                UpdatePlaceList(10, place);
                af10.setError(null);
            }

            @Override
            public void onError(Status status) {

            }
        });

        af1 = ((EditText) autocompleteFragment1.getView().findViewById(R.id.place_autocomplete_search_input));
        af2 = ((EditText) autocompleteFragment2.getView().findViewById(R.id.place_autocomplete_search_input));
        af3 = ((EditText) autocompleteFragment3.getView().findViewById(R.id.place_autocomplete_search_input));
        af4 = ((EditText) autocompleteFragment4.getView().findViewById(R.id.place_autocomplete_search_input));
        af5 = ((EditText) autocompleteFragment5.getView().findViewById(R.id.place_autocomplete_search_input));
        af6 = ((EditText) autocompleteFragment6.getView().findViewById(R.id.place_autocomplete_search_input));
        af7 = ((EditText) autocompleteFragment7.getView().findViewById(R.id.place_autocomplete_search_input));
        af8 = ((EditText) autocompleteFragment8.getView().findViewById(R.id.place_autocomplete_search_input));
        af9 = ((EditText) autocompleteFragment9.getView().findViewById(R.id.place_autocomplete_search_input));
        af10 = ((EditText) autocompleteFragment10.getView().findViewById(R.id.place_autocomplete_search_input));
    }

    public void SubmitLocations(View v) {

        boolean valid = true;

        if (ValidateYelpSubmit()) {

            double radius = .1;
            double longSum = 0;
            double latSum = 0;

            for (int i = 0; i < latLngList.size(); i++) {
                LatLng temp = latLngList.get(i).latLng;
                longSum += temp.longitude;
                latSum += temp.latitude;
            }

            double avgLongitude = longSum / latLngList.size();
            double avgLatitude = latSum / latLngList.size();
            double radiusModifier = radius / Math.sqrt(2);

            SW_Latitude = avgLatitude - radiusModifier;
            SW_Longitude = avgLongitude - radiusModifier;
            NE_Latitude = avgLatitude + radiusModifier;
            NE_Longitude = avgLongitude + radiusModifier;

            SubmitYelp();
            //showNext();
        }
    }

    EditText af1;
    EditText af2;
    EditText af3;
    EditText af4;
    EditText af5;
    EditText af6;
    EditText af7;
    EditText af8;
    EditText af9;
    EditText af10;

    public boolean ValidateYelpSubmit() {
        boolean valid = true;

        if (autocompleteFragmentContainer1.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(1))) {
                af1.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if (autocompleteFragmentContainer2.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(2))) {
                af2.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if (autocompleteFragmentContainer3.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(3))) {
                af3.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if (autocompleteFragmentContainer4.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(4))) {
                af4.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if (autocompleteFragmentContainer5.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(5))) {
                af5.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if (autocompleteFragmentContainer6.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(6))) {
                af6.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if (autocompleteFragmentContainer7.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(7))) {
                af7.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if (autocompleteFragmentContainer8.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(8))) {
                af8.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if (autocompleteFragmentContainer9.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(9))) {
                af9.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if (autocompleteFragmentContainer10.getVisibility() == View.VISIBLE){
            if (!latLngList.contains(new PlaceContainer(10))) {
                af10.setError("Please select a value from the place autocomplete for this location.");
                valid = false;
            }
        }

        if(TextUtils.isEmpty(yelpQuery.getText().toString())) {
            yelpQuery.setError("You must enter at least one item to search for.");
            valid = false;
        }

        return  valid;
    }

    EditText yelpQuery;
    RangeBar ratingBar;
    String rating = "";
    RangeBar priceBar;
    String price = "";
    ContentYelpResultsBinding binding;

    public void SubmitYelpQuery(View v) {
        SubmitYelp();
    }

    public void SubmitYelp(){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Searching Yelp");
        dialog.setMessage("Please Wait while we search...");
        dialog.setCancelable(false);
        dialog.show();

        hideSoftKeyboard(this);
        Map<String, String> params = new HashMap<>();

        params.put("term", yelpQuery.getText().toString());

        if(!TextUtils.isEmpty(rating)) {
            params.put("rating",  rating);
        }

        if(!TextUtils.isEmpty(price)) {
            params.put("price",  price);
        }

        params.put("limit", "20");
        params.put("radius_filter","805");
        params.put("sort","1");

        BoundingBoxOptions bounds = BoundingBoxOptions.builder()
                .swLatitude(SW_Latitude)
                .swLongitude(SW_Longitude)
                .neLatitude(NE_Latitude)
                .neLongitude(NE_Longitude).build();

        Call<SearchResponse> call = yelpAPI.search(bounds, params);

        try {
            Callback<SearchResponse> callback = new Callback<SearchResponse>() {
                @Override
                public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                    SearchResponse searchResponse = response.body();
                    YelpResultList list = YelpResult.UpdateYelpResultViewModel(searchResponse.businesses());

                    if(binding == null) {
                        View view = findViewById(R.id.include_content_yelp_results);

                        binding = ContentYelpResultsBinding.bind(view);
                    }

                    binding.setResult(list);
                    dialog.dismiss();
                    showNext();
                }
                @Override
                public void onFailure(Call<SearchResponse> call, Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "An error has occurred, please make sure all fields are filled in", Toast.LENGTH_SHORT);
                    Logger.Instance().appendLog(t.getMessage(), getApplicationContext());
                }
            };

            call.enqueue(callback);
        }
        catch (Exception exception)
        {
            dialog.dismiss();
            Toast.makeText(getApplicationContext(), "An error has occurred, please make sure all fields are filled in", Toast.LENGTH_SHORT);
            Logger.Instance().appendLog(exception.getMessage(), getApplicationContext());
        }
    }

    private void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);

        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    ArrayList<PlaceContainer> latLngList = new ArrayList<PlaceContainer>();
    public void UpdatePlaceList(int index, Place place){
        LatLng latLng = place.getLatLng();
        latLngList.add(new PlaceContainer(index, latLng));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            if(flipper.getChildAt(0) != flipper.getCurrentView())
            {
                showPrevious();
            }
            else{
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_search) {
            Intent intent = new Intent(this, SearchLocation.class);
            finish();
            startActivity(intent);

        }  else if (id == R.id.nav_faq) {
            DialogFragment frag = WebViewDialog.newInstance("FAQ", "http://www.middlefy.com/Home/Faq?noBar=true");
            frag.show(YelpController.Instance().CurrentFragmentManager, "dialog");
        } else if (id == R.id.nav_privacy) {
            DialogFragment frag = WebViewDialog.newInstance("Privacy Policy", "http://www.middlefy.com/Home/Privacy?noBar=true");
            frag.show(YelpController.Instance().CurrentFragmentManager, "dialog");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
